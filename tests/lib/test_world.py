import unittest
from lib.world import World

#
# The MIT License (MIT)
# Copyright (c) 2016 Jan Henrik Hasselberg
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

class TestWorld(unittest.TestCase):

    def setUp(self):
        self.world = World()

    def test_canary_test(self):
        self.assertTrue(True)
        self.assertIsInstance(self.world, World)

    def test_world_is_x_width(self):
        self.assertTrue(self.world.x_width is 50, "Width of world wasn't as expected")

    def test_world_is_y_heigth(self):
        self.assertTrue(self.world.y_height == 50, "Height of world wasn't as expected")

    def test_world_has_set_for_live_cells(self):
        self.assertTrue(hasattr(self.world, 'live_cells'), "World don't have live_cells set attribute")
        self.assertIsInstance(self.world.live_cells, set)

    def test_world_supports_setting_initial_set_of_live_cells(self):
        self.assertIs(len(self.world.live_cells), 0, "Default number of cells wasn't zero")
        live_cells = set([(1,2), (2,1)])
        self.assertIs(len(World(live_cells=live_cells).live_cells), 2, "Couldn't initialize with set values of live cells")

    def test_autopopulate_world(self):
        self.assertTrue(hasattr(self.world, 'autopopulate'), "World doesn't have autopopulate attribute")
        self.assertTrue(callable(getattr(self.world, 'autopopulate')), "World doesn't have autopopulate method")
        self.world.autopopulate()
        self.assertTrue(len(self.world.live_cells) > 0, "World didn't autopopulate")

    def test_world_can_be_erased(self):
        sample_set = set([(3,3), (3,4)])
        self.world.live_cells = sample_set
        self.world.erase()
        self.assertIsNot(sample_set, self.world.live_cells, "Cells didn't get erased.")
        self.assertTrue(len(self.world.live_cells) == 0, "World didn't get completely erased.")

    def test_add_single_cell_to_the_world(self):
        self.assertEqual(len(self.world.live_cells), 0)
        self.world.set(3,6)
        self.assertEqual(len(self.world.live_cells), 1)

    def test_add_single_cell_outside_world_bounderies_should_fail(self):
        x = self.world.x_width + 1
        y = self.world.y_height * -1
        with self.assertRaises(ValueError):
            self.world.set(x, y)
        with self.assertRaisesRegex(ValueError, '^The coordinates -*\d+ and -*\d+', msg="The error message wasn't what was expected"):
            self.world.set(x, y)

    def test_world_can_check_if_cell_coordinate_is_legal(self):
        x_not_legal = self.world.x_width + 1
        y_not_legal = self.world.y_height * -1
        self.assertFalse(self.world.is_legal(x_not_legal, y_not_legal), "Illegal coordinates found legal")
        x_legal = 1
        y_legal = self.world.y_height - 1
        self.assertTrue(self.world.is_legal(x_legal, y_legal), "Legal coordinates found illegal")

    def test_world_can_count_neighbors_of_a_cell(self):
        s = self.world.set
        x = 2 # Target cell x coordinate.
        y = 2 # Target cell y coordinate.
        s(x,y)
        self.assertEqual(self.world.count_neighbors(x,y), 0)
        s(1,1)
        self.assertEqual(self.world.count_neighbors(x,y), 1)
        s(1,2)
        self.assertEqual(self.world.count_neighbors(x,y), 2)
        s(1,3)
        self.assertEqual(self.world.count_neighbors(x,y), 3)
        s(2,1)
        self.assertEqual(self.world.count_neighbors(x,y), 4)
        s(2,3)
        self.assertEqual(self.world.count_neighbors(x,y), 5)
        s(3,1)
        self.assertEqual(self.world.count_neighbors(x,y), 6)
        s(3,2)
        self.assertEqual(self.world.count_neighbors(x,y), 7)
        s(3,3)
        self.assertEqual(self.world.count_neighbors(x,y), 8)

    def test_world_correctly_count_neighbors_even_at_edge_of_map(self):
        s = self.world.set

        # Test case with x and y equal 0.
        x = 0 # Target cell x coordinate.
        y = 0 # Target cell y coordinate.
        s(x,y)
        self.assertEqual(self.world.count_neighbors(x,y), 0)
        s(self.world.x_width, self.world.y_height)
        self.assertEqual(self.world.count_neighbors(x,y), 1)
        s(0, self.world.y_height)
        self.assertEqual(self.world.count_neighbors(x,y), 2)
        s(self.world.x_width, 0)
        self.assertEqual(self.world.count_neighbors(x,y), 3)

        self.world.erase()

        # Test case with x and y equal maximum values.
        x = self.world.x_width # Target cell x coordinate.
        y = self.world.y_height # Target cell y coordinate.
        s(x,y)
        self.assertEqual(self.world.count_neighbors(x,y), 0)
        s(0, 0)
        self.assertEqual(self.world.count_neighbors(x,y), 1)
        s(0, self.world.y_height)
        self.assertEqual(self.world.count_neighbors(x,y), 2)
        s(self.world.x_width, 0)
        self.assertEqual(self.world.count_neighbors(x,y), 3)

    def test_world_generate_new_population_glider_pattern(self):
        """
        Glider is a famous game of life pattern.

        .O
        ..O => O.O
        OOO    .OO
               .O

        http://www.conwaylife.com/wiki/Glider
        """
        glider_step_one = set([
            (5, 5),
            (6, 6),
            (4, 7),
            (5, 7),
            (6, 7)
            ])
        glider_step_two = set([
            (4, 6),
            (6, 6),
            (5, 7),
            (6, 7),
            (5, 8)
            ])
        self.world.live_cells = glider_step_one
        self.world.generate_new_generation()
        self.assertEqual(self.world.live_cells, glider_step_two, "New population didn't match expected pattern")

    def test_world_generate_new_population_claw_with_tail(self):
        """
        Claw with tail is a still life pattern that doesn't change.

        OO        OO
        .O        .O
        .O.OO  => .O.OO
        ..O..O    ..O..O
        ....OO    ....OO

        http://www.conwaylife.com/wiki/Claw_with_tail
        """
        claw_step_one = set([
            (2,2),
            (3,2),
            (3,3),
            (3,4),
            (5,4),
            (6,4),
            (4,5),
            (7,5),
            (6,6),
            (7,6)
            ])
        claw_step_two = set([
            (2,2),
            (3,2),
            (3,3),
            (3,4),
            (5,4),
            (6,4),
            (4,5),
            (7,5),
            (6,6),
            (7,6)
            ])
        self.world.live_cells = claw_step_one
        self.world.generate_new_generation()
        self.assertEqual(self.world.live_cells, claw_step_two, "New population didn't match expected pattern")


