import turtle, sys
from turtle import TK
from lib.world import World

#
# Turtle GUI implementation is based on:
# https://fiftyexamples.readthedocs.io/en/latest/life.html
# https://github.com/akuchling/50-examples/blob/18b6802c40787e70b289cd8614b55db8f14c9a16/life.rst
# Authored by akuchling at github.com.
# Reused here for educational purposes.
#
# The rest of the files in this repository,
# copyright Jan Henrik Hasselberg, "MIT license".
#

CELL_SIZE=10 # Pixels

def display_help_window():
    root = TK.Tk()
    frame = TK.Frame()
    canvas = TK.Canvas(root, width=300, height=200, bg="white")
    canvas.pack()
    help_screen = turtle.TurtleScreen(canvas)
    help_t = turtle.RawTurtle(help_screen)
    help_t.penup()
    help_t.hideturtle()
    help_t.speed('fastest')

    width, height = help_screen.screensize()
    line_height = 20
    y = height // 2 - 30
    for s in ("Click on cells to make them alive or dead.",
            "Keyboard commands:",
            " E)rase the board",
            " R)andom fill",
            " S)tep once or",
            " C)ontinuously -- use 'S' to resume stepping",
            " Q)uit"):
        help_t.setpos(-(width / 2), y)
        help_t.write(s, font=('sans-serif', 14, 'normal'))
        y -= line_height

def draw(board, x, y):
    "Update the cell (x,y) on the display."
    turtle.penup()
    key = (x, y)
    if key in board.live_cells:
        turtle.setpos(x*CELL_SIZE, y*CELL_SIZE)
        turtle.color('black')
        turtle.pendown()
        turtle.setheading(0)
        turtle.begin_fill()
        for i in range(4):
            turtle.forward(CELL_SIZE-1)
            turtle.left(90)
        turtle.end_fill()

def display(board):
    """Draw the whole board"""
    turtle.clear()
    for i in range(board.x_width):
        for j in range(board.y_height):
            draw(board, i, j)
    turtle.update()

def main():
    display_help_window()
    scr = turtle.Screen()
    turtle.mode('standard')
    xsize, ysize = scr.screensize()
    turtle.setworldcoordinates(0, 0, xsize, ysize)

    turtle.hideturtle()
    turtle.speed('fastest')
    turtle.tracer(0, 0)
    turtle.penup()

    board = World(x_width=xsize // CELL_SIZE, y_height=1 + ysize // CELL_SIZE)

    # Set up mouse bindings
    def toggle(x, y):
        cell_x = x // CELL_SIZE
        cell_y = y // CELL_SIZE
        if board.is_legal(cell_x, cell_y):
            # Remove live cell if exists, or add if not exists.
            if (cell_x, cell_y) in board.live_cells:
                board.live_cells.remove((cell_x, cell_y))
            else:
                board.set(cell_x, cell_y)
            display(board)

    turtle.onscreenclick(turtle.listen)
    turtle.onscreenclick(toggle)

    board.autopopulate()
    display(board)


    # Set up key bindings
    def erase():
        board.erase()
        display(board)
    turtle.onkey(erase, 'e')

    def makeRandom():
        board.autopopulate()
        display(board)
    turtle.onkey(makeRandom, 'r')

    # Ok so far..
    turtle.onkey(sys.exit, 'q')

    # Set up keys for performing generation steps, either one-at-a-time or not.
    continuous = False
    def step_once():
        nonlocal continuous
        continuous = False
        perform_step()

    def step_continuous():
        nonlocal continuous
        continuous = True
        perform_step()

    def perform_step():
        board.generate_new_generation()
        display(board)
        # In continuous mode, we set a timer to display another generation
        # after 25 millisenconds.
        if continuous:
            turtle.ontimer(perform_step, 25)

    turtle.onkey(step_once, 's')
    turtle.onkey(step_continuous, 'c')

    # Enter the Tk main loop
    turtle.listen()
    turtle.mainloop()

if __name__ == "__main__":
    main()
