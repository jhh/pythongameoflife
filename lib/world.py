import random

#
# The MIT License (MIT)
# Copyright (c) 2016 Jan Henrik Hasselberg
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

class World():

    def __init__(self, x_width=50, y_height=50, live_cells=None):
        # Size of world.
        self.x_width=x_width
        self.y_height=y_height

        self.erase() # Initialize self.live_cells.
        if live_cells is not None:
            self.live_cells=live_cells

    def autopopulate(self):
        self.erase()
        for y in range(0, self.y_height):
            for x in range(0, self.x_width):
                if random.random() > 0.5:
                    self.set(x, y)

    def erase(self):
        self.live_cells = set()

    def set(self, x, y):
        if self.is_legal(x, y):
            self.live_cells.add((x, y))
        else:
            raise ValueError("The coordinates {x} and {y} is outside scope of the world map.".format(x=x, y=y))

    def is_legal(self, x, y):
        return 0 <= x <= self.x_width and 0 <= y <= self.y_height

    def count_neighbors(self, x, y):
        if not self.is_legal(x, y):
            raise ValueError("The coordinates {x} and {y} is outside scope of the world map.".format(x=x, y=y))
        neighbors_count = 0
        for y_for in range(y-1, y+2):
            for x_for in range(x-1, x+2):
                if x_for == x and y_for == y:
                    continue # Skip the target cell itself.
                neighbour_x = self._count_neighbors_wrap_edges(x_for, 0, self.x_width)
                neighbour_y = self._count_neighbors_wrap_edges(y_for, 0, self.y_height)
                if (neighbour_x, neighbour_y) in self.live_cells:
                    neighbors_count+=1
        return neighbors_count

    def _count_neighbors_wrap_edges(self, n, min, max):
        result = n
        if min > n:
            result = max
        elif max < n:
            result = 0
        return result

    def generate_new_generation(self):
        new_generation = set()
        for y_for in range(0, self.y_height+1):
            for x_for in range(0, self.x_width+1):
                cell_is_alive = (x_for, y_for) in self.live_cells
                number_neighbors = self.count_neighbors(x_for, y_for)
                if cell_is_alive:
                    # Any live cell with fewer than two live neighbours dies, as if caused by under-population.
                    # Any live cell with two or three live neighbours lives on to the next generation.
                    if 2 <= number_neighbors <= 3:
                        new_generation.add((x_for, y_for)) # Survives.
                    # Any live cell with more than three live neighbours dies, as if by over-population.
                else:
                    # Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                    if number_neighbors == 3:
                        new_generation.add((x_for, y_for)) # New life.
        self.live_cells = new_generation

