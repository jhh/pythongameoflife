# README #

Lab tasks made by me for introducing TDD in Python 3 to the team.

This repository contains both lab notes and a solution to the task.

Feel free to reuse, MIT license applies. Some code in game.py is borrowed, different license might apply.